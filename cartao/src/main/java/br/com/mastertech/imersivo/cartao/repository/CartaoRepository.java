package br.com.mastertech.imersivo.cartao.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.mastertech.imersivo.cartao.model.Cartao;

@Repository
public interface CartaoRepository extends CrudRepository<Cartao, Long> {

	Cartao findByNumero(String numero);

}

